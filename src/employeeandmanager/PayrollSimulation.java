/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeeandmanager;

/**
 *
 * @author sahej
 */
public class PayrollSimulation {

   public static void main(String[] args) {

       EmployeeFactory employeeFactory = EmployeeFactory.getInstance();
       Employee employee = employeeFactory.getEmployee(Type.MANAGER);
       System.out.println(employee.describeEmployee());
       System.out.println(employee.calculatePay());
       Employee employee2 = employeeFactory.getEmployee(Type.EMPLOYEE);
       System.out.println(employee2.describeEmployee());
       System.out.println(employee2.calculatePay());

   }

}